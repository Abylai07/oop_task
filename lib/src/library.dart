import 'package:oop/src/reviews_searchable.dart';
import 'media_item.dart';

class Library implements Searchable {

  List<MediaItem> library = [];

  BookMedia bookMedia = BookMedia(author: 'Abay Kunanbay', isbn: '123213-23132', id: 1);
  DvdMedia dvdMedia = DvdMedia(director: 'Director', duration: const Duration(minutes: 20), id: 2);
  JournalMedia journalMedia = JournalMedia(volume: 1, issueNumber: 2321, id: 3);

  addCustomBooks(){
    library.addAll([bookMedia, dvdMedia, journalMedia]);
  }

  @override
  searchByTitle(String title) {
    List<MediaItem> searchList = [];
    for (var element in library) {
      if (element.title == title) {
        searchList.add(element);
      }
    }
    return searchList;
  }

  addToLibrary(MediaItem item) {
    library.add(item);
  }

  checkedOut(MediaItem item) {
    return item.isCheckedOut = !item.isCheckedOut;
  }

  getLibrary() {
    return library;
  }
}
