
interface class Searchable {
  searchByTitle(String title) {}
}

mixin Reviews {
  List<ReviewModel> reviews = [];
  addReview(int id, String review){
    reviews.add(ReviewModel(id: id, review: review));
  }

  viewReviews(int? id){
    if(id == null){
      if(reviews.isEmpty){
        print('Reviews is empty');
      } else {
        print('Reviews:');
        for(final item in reviews){
          print('id: ${item.id}, review: ${item.review}');
        }
      }
    } else {
      print('Reviews by ID:');
      for(final item in reviews){
        if(item.id == id){
          print('id: ${item.id}, review: ${item.review}');
        }
      }
    }
  }
}

class ReviewModel {
  final int id;
  String? review;

  ReviewModel({required this.id, this.review});
}