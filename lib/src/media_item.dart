import 'package:oop/src/reviews_searchable.dart';

abstract class MediaItem {
  int id;
  String? title;
  int? publishYear;
  bool isCheckedOut;

  MediaItem(
      {required this.id,
        this.title,
        this.publishYear,
        this.isCheckedOut = false});

  getMedia() {
    print('get Media');
  }

  setMedia(BookMedia bookMedia) {
    print('set new Media');
    id = bookMedia.id;
    title = bookMedia.title;
    publishYear = bookMedia.publishYear;
    isCheckedOut = bookMedia.isCheckedOut;
  }

}

class BookMedia extends MediaItem with Reviews {
  String? author;
  String? isbn;
  BookMedia({this.author, this.isbn, required super.id, super.title, super.publishYear, super.isCheckedOut});

  @override
  getMedia() {
    super.getMedia();
    return BookMedia(id: id, title: title, author: author);
  }

  @override
  setMedia(BookMedia bookMedia) {
    super.setMedia(bookMedia);
    author = bookMedia.author;
    isbn = bookMedia.isbn;
  }
}

class DvdMedia extends MediaItem with Reviews {
  String? director;
  Duration? duration;
  DvdMedia({this.director, this.duration, required super.id, super.title, super.publishYear, super.isCheckedOut});
}

class JournalMedia extends MediaItem with Reviews {
  int? volume;
  int? issueNumber;

  JournalMedia({this.volume, this.issueNumber, required super.id, super.title, super.publishYear, super.isCheckedOut});
}